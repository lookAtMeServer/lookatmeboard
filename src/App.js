import React from 'react';
import './App.css';
import Error from './components/general/Error'
import ScreensConst from './components/constant/Screens'
import LogInSmartScreen from './screens/LogInSmartScreen'
import AdvertisementScreen from './screens/AdvertisementScreen'
import Api from './assets/Api'
import io from 'socket.io-client'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


const testModeFlag = false

class App extends React.Component {
  constructor(props) {
    super()
    this.state = {
      error: false,
      screen: ScreensConst.LOGIN,
      digitalBoardInfo: false,
      consumer: false,
      socket: false,
      consumerPicture: false
    }
    this.mountFlag = false
  }

  componentWillUnmount() {
    this.mountFlag = false
  }


  componentDidMount() {
    this.mountFlag = true
    if (testModeFlag) {
      this.handleLogInSmartScreen({
        digitalBoardId: 7,
        digitalBoardAddress: 'בוגרשוב/בר כוכבא, תל אביב יפו, ישראל',
        error: false
      })

    }

  }

  handleTopMenuClick = (clickEvent) => {
  }

  handleError = (error) => {
    if (error == false && this.mountFlag) {
      this.setState({
        error: false
      })
    } else {
      if (this.mountFlag) {

        this.setState({
          error: error.toString()
        })

      }
    }
  }

  handleLogInSmartScreen = async (digitalBoardInfo) => {
    try {
      if (this.mountFlag) {
        await new Promise(resolved => {
          resolved(this.setState({
            digitalBoardInfo: digitalBoardInfo,
            socket: io('http://lookatmeil.com/'),
            screen: ScreensConst.ADVERTISEMENTS
          })
          )
        })
      }

      this.state.socket.on('findBoardForConsumer', (data) => {
        if (this.state.digitalBoardInfo) {
          if (this.state.digitalBoardInfo.digitalBoardId == data.digitalBoardId && !this.state.consumer) {
            this.state.socket.emit('freeSmartScreen', { consumerEmail: data.consumerEmail })
          }
        }
      })

      this.state.socket.on('logOffConsumer', (data) => {
        if (this.state.consumer) {
          if (typeof data.consumerEmail != 'undefined') {
            if (this.state.consumer == data.consumerEmail && this.mountFlag) {
              this.setState({
                consumer: false,
                consumerPicture: false
              })
            }
          }
        }
      })
      this.state.socket.on('connectConsumer', (data) => {
        if (!this.state.consumer && this.mountFlag) {
          console.log(data)
          this.setState({
            consumer: data.consumerEmail,
            consumerPicture: typeof data.userPictureUri != 'undefined' ? data.userPictureUri : false
          })
        }
      })
      this.state.socket.on('disconnect', () => {
        this.state.socket.close()
        if (this.mountFlag) {

          this.setState({
            consumer: false,
            socket: false,
            screen: ScreensConst.LOGIN,
            digitalBoardInfo: false,
            consumerPicture: false

          })

        }
      })

    } catch (error) {
      this.handleError(error)
    }
  }

  getDigitalBoardInfo = () => {
    return this.state.digitalBoardInfo
  }

  selectScreen = () => {
    if (this.state.screen == ScreensConst.LOGIN) {
      return <LogInSmartScreen
        getDigitalBoardInfo={this.getDigitalBoardInfo}
        handleLogInSmartScreen={this.handleLogInSmartScreen}
      />
    } else if (this.state.screen == ScreensConst.ADVERTISEMENTS) {
      return <AdvertisementScreen
        consumerEmail={this.state.consumer ? this.state.consumer : false}
        digitalBoardInfo={this.state.digitalBoardInfo}
        handleConsumerLogOff={this.handleConsumerLogOff}
        consumerPicture={this.state.consumerPicture ? this.state.consumerPicture : false}
      />

    }
  }

  handleConsumerLogOff = async (consumerEmail) => {
    try {
      if (this.state.consumer == consumerEmail && this.mountFlag) {
        let getAllCitiesResponed = await Api.logOffConsumerFromSmartScreen(this.state.consumer)
        this.setState({
          consumer: false,
          consumerPicture: false

        })
      }
    } catch (error) {
      if (this.mountFlag) {

        this.setState({
          error: error.toString(),
          consumer: false,
          consumerPicture: false
        })

      }
    }

  }

  render() {
    return (
      <Router>
        <div className="App">




          <Switch>

            <Route path="/">
              {this.state.error ? <Error errorText={this.state.error} /> : null}
              {this.state.screen == ScreensConst.LOGIN ?
                <LogInSmartScreen
                  getDigitalBoardInfo={this.getDigitalBoardInfo}
                  handleLogInSmartScreen={this.handleLogInSmartScreen}
                />
                :
                <section id='advertisements-box'>
                  <AdvertisementScreen
                    consumerEmail={this.state.consumer ? this.state.consumer : false}
                    digitalBoardInfo={this.state.digitalBoardInfo}
                    handleConsumerLogOff={this.handleConsumerLogOff}
                    consumerPicture={this.state.consumerPicture ? this.state.consumerPicture : false}
                  />


                </section>

              }
            </Route>


          </Switch>
        </div>
      </Router>




    );
  }
}

export default App;

import Api from '../assets/Api'



class DigitalBoard {
    constructor(id = false) {
        this.id = id
        this.randomColors = []
    }

    setDigitalBoardId = (id) => {
        this.id = id
    }


    setRandomColor = (categoryId) => {
        let categoryIndex = this.randomColors.find(item => item.categoryId == categoryId)
        if (categoryIndex) {
            return categoryIndex.color
        } else {
            let letters = '0123456789ABCDEF';
            let color = '#';
            for (let i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            color += '44'
            this.randomColors.push({ categoryId: categoryId, color: color })
            return color
        }
    }

    filterCategories = async (advertisements) => {
        let categories = advertisements.filter((item, index) => advertisements.findIndex(item2 => item2.categoryId == item.categoryId) == index)
        let temp = []
        categories.forEach(item => {
            temp.push({
                categoryId: item.categoryId,
                categoryText: item.categoryText
            })
        })
        return [...temp]
    }

    getAdvertisements = async () => {
        try {
            if (this.id != false) {
                let responed = await Api.getAdvertisementForSmartScreen(this.id, false)
                responed.value.sort((a, b) => a.categoryId - b.categoryId)
                responed.value.forEach((ad, index, arr) => {
                    arr[index].color = this.setRandomColor(ad.categoryId)
                })
                return [...responed.value]
            } else {
                throw 'set id for DigitalBoard'
            }
        } catch (error) {
            throw error
        }

    }

    getMatchAdsForConsumer = async (consumerEmail) => {
        try {
            if (this.id != false) {
                let advertisements = await Api.getAdvertisementForSmartScreen(this.id, consumerEmail)
                if (typeof advertisements.value.weight != 'undefined') {
                    advertisements.value.sort((a, b) => a.weight - b.weight)
                    advertisements.value.sort((a, b) => a.categoryId - b.categoryId)
                } else {
                    advertisements.value.sort((a, b) => a.categoryId - b.categoryId)
                }
                advertisements.value.forEach((ad, index, arr) => {
                    arr[index].color = this.setRandomColor(ad.categoryId)
                })
                return [...advertisements.value]
            } else {
                throw 'set id for DigitalBoard'
            }
        } catch (error) {
            throw error
        }

    }

    filterAllCategories = (advertisements, matchAdsForConsumer) => {
        try {
            matchAdsForConsumer.forEach(item => {
                let index = advertisements.findIndex(item2 => item2.id == item.id)
                if (index>-1) {
                    advertisements.splice(index, 1)
                }
            })
        } catch (error) {
            throw error
        }
    }

    setMatchAds = (advertisements, matchAdsForConsumer) => {
        try {
            advertisements.forEach((item, index, arr) => {
                if (typeof item.weight != 'undefined') {
                    arr[index].match = true
                } else {
                    arr[index].match = false
                }
            })
            matchAdsForConsumer.forEach((item, index, arr) => {
                if (typeof item.weight != 'undefined') {
                    arr[index].match = true
                } else {
                    arr[index].match = false
                }
            })
        } catch (error) {
            throw error
        }
    }


    setConsumerSelectedAds = (consumerSelectedAds, advertisements, matchAdsForConsumer) => {
        try {
            advertisements.forEach((item, index, arr) => {
                if (consumerSelectedAds.value.findIndex(item2 => item2.advertisementId == item.id) > -1) {
                    arr[index].consumerAd = true
                } else {
                    arr[index].consumerAd = false
                }
            })
            matchAdsForConsumer.forEach((item, index, arr) => {
                if (consumerSelectedAds.value.findIndex(item2 => item2.advertisementId == item.id) > -1) {
                    arr[index].consumerAd = true
                } else {
                    arr[index].consumerAd = false
                }
            })
        } catch (error) {
            throw error
        }
    }
    selectCategory = (selectCategory, filterAdvertisements, filterConsumerAdvertisements, advertisements, matchAdsForConsumer, consumerEmail = false) => {
        try {
            if (consumerEmail) {
                matchAdsForConsumer.forEach((item, index, arr) => {
                    if (item.categoryId == selectCategory) {
                        filterConsumerAdvertisements.push(item)
                    }
                })
            }
            advertisements.forEach((item, index, arr) => {
                if (item.categoryId == selectCategory) {
                    filterAdvertisements.push(item)
                }
            })
        } catch (error) {
            throw error
        }
    }

    secondFilterMatch = (matchAds) => {
        try {
            let machTempArray = [...matchAds]
            let categories = machTempArray.filter((item, index) => machTempArray.findIndex(item2 => item2.categoryId == item.categoryId) == index)
            let temp = []
            categories.sort((a, b) => a.weight - b.weight)


            matchAds.forEach((item,index,arr)=>{
                if(item.averageRank==null){
                    arr[index].averageRank=0
                }
            })

            for (let i = 0; i < categories.length; i++) {
                let averageRank = 0
                let views = 0
                let index = -1
                for (let x = 0; x < machTempArray.length; x++) {

                    if (machTempArray[x].categoryId == categories[i].categoryId) {
                        if (machTempArray[x].averageRank > averageRank) {
                            averageRank = machTempArray[x].averageRank
                            index = x
                            views = machTempArray[x].views
                        } else if (machTempArray[x].averageRank == averageRank) {
                            if (machTempArray[x].views >= views) {
                                averageRank = machTempArray[x].averageRank
                                index = x
                                views = machTempArray[x].views
                            }
                        }
                    }

                }


                if (index != -1) {
                    temp.push(machTempArray[index])
                    machTempArray.splice(index, 1)
                }


            }

            categories = machTempArray.filter((item, index) => machTempArray.findIndex(item2 => item2.categoryId == item.categoryId) == index)

            categories.sort((a, b) => a.weight - b.weight)

            for (let i = 0; i < categories.length; i++) {
                let averageRank = 0
                let views = 0
                let index = -1
                for (let x = 0; x < machTempArray.length; x++) {

                    if (machTempArray[x].categoryId == categories[i].categoryId) {
                        if (machTempArray[x].averageRank > averageRank) {
                            averageRank = machTempArray[x].averageRank
                            index = x
                            views = machTempArray[x].views
                        } else if (machTempArray[x].averageRank == averageRank) {
                            if (machTempArray[x].views >= views) {
                                averageRank = machTempArray[x].averageRank
                                index = x
                                views = machTempArray[x].views
                            }
                        }
                    }

                }


                if (index != -1) {
                    temp.push(machTempArray[index])
                    machTempArray.splice(index, 1)
                }


            }


            categories = machTempArray.filter((item, index) => machTempArray.findIndex(item2 => item2.categoryId == item.categoryId) == index)

            categories.sort((a, b) => a.weight - b.weight)

            for (let i = 0; i < categories.length; i++) {
                let averageRank = 0
                let views = 0
                let index = -1
                for (let x = 0; x < machTempArray.length; x++) {

                    if (machTempArray[x].categoryId == categories[i].categoryId) {
                        if (machTempArray[x].averageRank > averageRank) {
                            averageRank = machTempArray[x].averageRank
                            index = x
                            views = machTempArray[x].views
                        } else if (machTempArray[x].averageRank == averageRank) {
                            if (machTempArray[x].views >= views) {
                                averageRank = machTempArray[x].averageRank
                                index = x
                                views = machTempArray[x].views
                            }
                        }
                    }

                }


                if (index != -1) {
                    temp.push(machTempArray[index])
                    machTempArray.splice(index, 1)
                }


            }

            for (let i = 0; i < machTempArray.length; i++) {
                temp.push(machTempArray[i])
            }

            return temp
  

        } catch (error) {
            throw error
        }
    }
    secondFilterAds = (advertisements) => {
        try {
            let machTempArray = [...advertisements]
            let categories = machTempArray.filter((item, index) => machTempArray.findIndex(item2 => item2.categoryId == item.categoryId) == index)
            let temp = []
         
            machTempArray.forEach((item,index,arr)=>{
                if(item.averageRank==null){
                    arr[index].averageRank=0
                }
            })

            for (let i = 0; i < categories.length; i++) {
                let averageRank = 0
                let views = 0
                let index = -1
                for (let x = 0; x < machTempArray.length; x++) {

                    if (machTempArray[x].categoryId == categories[i].categoryId) {
                        if (machTempArray[x].averageRank > averageRank) {
                            averageRank = machTempArray[x].averageRank
                            index = x
                            views = machTempArray[x].views
                        } else if (machTempArray[x].averageRank == averageRank) {
                            if (machTempArray[x].views >= views) {
                                averageRank = machTempArray[x].averageRank
                                index = x
                                views = machTempArray[x].views
                            }
                        }
                    }

                }


                if (index != -1) {
                    temp.push(machTempArray[index])
                    machTempArray.splice(index, 1)
                }


            }

            categories = machTempArray.filter((item, index) => machTempArray.findIndex(item2 => item2.categoryId == item.categoryId) == index)


            for (let i = 0; i < categories.length; i++) {
                let averageRank = 0
                let views = 0
                let index = -1
                for (let x = 0; x < machTempArray.length; x++) {

                    if (machTempArray[x].categoryId == categories[i].categoryId) {
                        if (machTempArray[x].averageRank > averageRank) {
                            averageRank = machTempArray[x].averageRank
                            index = x
                            views = machTempArray[x].views
                        } else if (machTempArray[x].averageRank == averageRank) {
                            if (machTempArray[x].views >= views) {
                                averageRank = machTempArray[x].averageRank
                                index = x
                                views = machTempArray[x].views
                            }
                        }
                    }

                }


                if (index != -1) {
                    temp.push(machTempArray[index])
                    machTempArray.splice(index, 1)
                }


            }


            categories = machTempArray.filter((item, index) => machTempArray.findIndex(item2 => item2.categoryId == item.categoryId) == index)


            for (let i = 0; i < categories.length; i++) {
                let averageRank = 0
                let views = 0
                let index = -1
                for (let x = 0; x < machTempArray.length; x++) {

                    if (machTempArray[x].categoryId == categories[i].categoryId) {
                        if (machTempArray[x].averageRank > averageRank) {
                            averageRank = machTempArray[x].averageRank
                            index = x
                            views = machTempArray[x].views
                        } else if (machTempArray[x].averageRank == averageRank) {
                            if (machTempArray[x].views >= views) {
                                averageRank = machTempArray[x].averageRank
                                index = x
                                views = machTempArray[x].views
                            }
                        }
                    }

                }


                if (index != -1) {
                    temp.push(machTempArray[index])
                    machTempArray.splice(index, 1)
                }


            }

            for (let i = 0; i < machTempArray.length; i++) {
                temp.push(machTempArray[i])
            }

            return temp


        } catch (error) {
            throw error
        }
    }
}

let db = new DigitalBoard()

export default db
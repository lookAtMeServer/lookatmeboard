const rp = require('request-promise');


export default {
    getAllDigitalBoards: async () => {
        var options = {
            let: 'http://lookatmeil.com/getalldigitalboards',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            if (typeof httpResponse.value.nearbyUser != 'undefined') {
                this.setState({
                    Advertisement: [...httpResponse.value.advertisements],
                    user: httpResponse.value.nearbyUser
                })
            } else {
                this.setState({
                    Advertisement: [...httpResponse.value]
                })
            }

        }
        catch (error) {
            throw error
        }
    },
    getAllCities: async () => {
        let options = {
            uri: 'http://lookatmeil.com/getAllCities',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },
    getAllDigitalBoards: async (cityId) => {
        let options = {
            uri: 'http://lookatmeil.com/getalldigitalboards',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                cityId: cityId
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },
    getAdvertisementForSmartScreen: async (digitalBoardId, consumerEmail = false) => {
        let options = {
            uri: 'http://lookatmeil.com/getadvertisementforsmartscreen',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                digitalBoardId: digitalBoardId,
                consumerEmail: consumerEmail
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },
    logOffConsumerFromSmartScreen: async (consumerEmail) => {
        let options = {
            uri: 'http://lookatmeil.com/logoffconsumerfromsmartscreen',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                consumerEmail: consumerEmail,
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },
    getConsumerSelectedAds: async (consumerEmail) => {
        let options = {
            uri: 'http://lookatmeil.com/getconsumerselectedads',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                email: consumerEmail,
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },
    deleteAdvertisementFormConsumer: async (consumerEmail, advertisementsId) => {
        let options = {
            uri: 'http://lookatmeil.com/deleteadvertisementformconsumer',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                email: consumerEmail,
                advertisementsId: advertisementsId
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },
    addAdvertisementForConsumer: async (consumerEmail, advertisementsId) => {

        let options = {
            uri: 'http://lookatmeil.com/addadvertisementforconsumer',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                email: consumerEmail,
                advertisementsId: advertisementsId
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },
    sendSignUpEmail: async (userEmail, advertisementsId) => {

        let options = {
            uri: 'http://lookatmeil.com/sendsignupemail',
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: {
                email: userEmail,
                advertisementId: advertisementsId
            },
            json: true
        }
        try {
            let httpResponse = await rp(options)
            return httpResponse
        }
        catch (error) {
            throw error
        }
    },

}
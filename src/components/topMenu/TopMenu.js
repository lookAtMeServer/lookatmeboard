import React from 'react'
import Menu, { SubMenu, Item as MenuItem } from 'rc-menu'
import 'rc-menu/assets/index.css'

class TopMenu extends React.Component {
    constructor(props){
        super()
    }

    handleClick=(clickEvent)=>{
        
        this.props.clickHandle(clickEvent)
    }

    render() {
        return (
            <Menu
                mode={['horizontal']}
                defaultOpenKeys={this.props.defaultOpenKeys}
                onClick={this.handleClick}
            >
                <MenuItem key={1}>רשום מכשיר</MenuItem>
            </Menu>
        );
    }
}

export default TopMenu
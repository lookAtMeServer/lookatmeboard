import React from 'react';
import Advertisement from './Advertisement'
import Slider from "react-slick";

const rp = require('request-promise');

class ShowPictures extends React.Component {
  constructor() {
    super()
    this.state = {
      Advertisement: [],
      user:false
    }
  }
  async componentDidMount() {
    var options = {
      uri: 'http://lookatmeil.com/getadvertisementfordigitalboard?boardId=1',

      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      json: true
    }


    try {
      let httpResponse = await rp(options)
   
      if(typeof httpResponse.value.nearbyUser !='undefined'){
        this.setState({
            Advertisement: [...httpResponse.value.advertisements],
            user:httpResponse.value.nearbyUser
          })
      }else{
        this.setState({
            Advertisement: [...httpResponse.value]
          })
      }
      
    }

    catch (error) {
      console.log(error);

    }





  }
  render() {
   var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    
    return (
      <section>
        <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        {this.state.user?<p>{this.state.user.userEmail}</p>: null}
        <p>my name is niaar</p>
       <Slider {...settings}>
          {
            this.state.Advertisement.map(ad => <Advertisement key={ad.id.toString()} ad={ad} />)
          }
        </Slider>
      </section>

    )
  }
}

export default ShowPictures;
import React from 'react';
import './Advertisement.css'
import ClipLoader from "react-spinners/ClipLoader"
import Error from '../components/general/Error'
import Api from '../assets/Api'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import bookmark from '../assets/bookmark.png';
import bookmarkEmpty from '../assets/bookmarkEmpty.png';

const imageIntervalTime = 5000

class Advertisement extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            advertisements: [],
            advertisementImageIndex: 1,
            loading: false,
            error: false
        }
        this.mountFlag = false
    }
    componentDidMount() {
        this.mountFlag = true
        if (typeof this.props.ad.pictures != 'undefined') {
            this.setState({
                advertisements: [...this.props.ad.pictures],
                intervalId: setInterval(() => {
                    this.handleSwitchPicture()
                }, imageIntervalTime)
            })
        }
    }

    componentWillUnmount() {
        this.mountFlag = false
        clearInterval(this.state.intervalId)
    }

    handleSwitchPicture = () => {
        if (this.mountFlag) {

            this.setState({
                advertisementImageIndex: this.state.advertisementImageIndex + 1
            })

        }
    }

    handleSignUp = () => {
    }

    handleAddAdForConsumer = async () => {
        try {
            if (this.mountFlag) {
                this.setState({
                    loading: true
                })

            }
            let addAdvertisementForConsumerResponed = await Api.addAdvertisementForConsumer(this.props.consumerEmail, this.props.ad.id)
            await new Promise(resolved => {
                resolved(this.props.randerAdvertisements())
            })
            if (this.mountFlag) {

                this.setState({
                    loading: false
                })

            }
        } catch (error) {
            if (this.mountFlag) {

                this.setState({
                    error: error.toString(),
                    loading: false
                })

            }

        }

    }

    handleRemoveAdFromConsumer = async () => {
        try {
            if (this.mountFlag) {

                this.setState({
                    loading: true
                })

            }
            let deleteAdvertisementFormConsumerResponed = await Api.deleteAdvertisementFormConsumer(this.props.consumerEmail, this.props.ad.id)
            await new Promise(resolved => {
                resolved(this.props.randerAdvertisements())
            })
            if (this.mountFlag) {

                this.setState({
                    loading: false
                })

            }
        } catch (error) {
            if (this.mountFlag) {

                this.setState({
                    error: error.toString(),
                    loading: false
                })

            }

        }

    }

    registrationClick = async () => {
        if (typeof this.props.timerOnOff != 'undefined' && this.props.timerOnOff) {
            await this.props.timerOnOff(false)
            this.props.openSignUpModal(this.props.ad.id)

        } else {

            this.props.openSignUpModal(this.props.ad.id)

        }

    }

    render() {

        return (


            <section
                className={`advertisement-box ${this.props.match ? 'match-advertisement' : null}`}
                style={(typeof this.props.fullScreen != 'undefined' && this.props.fullScreen) ?
                    {
                        height: '90vh',
                        width: '100%',

                    }
                    :
                    {
                    }

                }

            >
                {this.state.error ? <Error errorText={this.state.error} /> : null}

                {this.state.advertisements.length > 0 ?
                    <section
                        onClick={this.props.openZoomInModal.bind(null, this.props.ad)}
                        className={(typeof this.props.fullScreen != 'undefined' && this.props.fullScreen) ? 'advertisement-image-box-full' : 'advertisement-image-box'}
                        style={{}}
                        style={{ backgroundImage: `url(http://lookatmeil.com/${this.state.advertisements[this.state.advertisementImageIndex % this.state.advertisements.length]})` }}
                    >
                    </section>
                    :
                    null
                }

                <section
                    className={(typeof this.props.fullScreen != 'undefined' && this.props.fullScreen) ? 'advertisement-info-full-screen' : 'advertisement-info'}

                >
                    {(typeof this.props.fullScreen != 'undefined' && this.props.fullScreen) ?
                        <section
                            style={{ fontSize: '3vh', fontWeight: 'bold', direction: 'rtl' }}
                        >
                            {this.props.ad.categoryText}
                        </section>
                        :
                        <section
                            style={{ fontSize: '2vh', fontWeight: 'bold', direction: 'rtl' }}
                        >
                            {this.props.ad.categoryText}
                        </section>


                    }
                    {(typeof this.props.fullScreen != 'undefined' && this.props.fullScreen) ?
                        <section
                            style={{ fontSize: '3vh', direction: 'rtl' }}
                        >
                            <span style={{ fontWeight: 'bold' }}>שם מקום:</span>  {this.props.ad.placeName}
                        </section>
                        :
                        <section
                            style={{ fontSize: '2vh', marginTop: 10, direction: 'rtl' }}
                        >
                            <span style={{ fontWeight: 'bold' }}>שם מקום:</span>  {this.props.ad.placeName}
                        </section>


                    }

         

                    <section

                        className={'advertisement-button'}
                        style={(typeof this.props.fullScreen != 'undefined' && this.props.fullScreen) ? { left:  '2.5vw', bottom: '5px' } : {left:typeof this.props.consumerAd !='undefined' ? this.props.consumerAd ?'-0.35vw' :'-0.2vw' : null}}
                    >

                        {this.props.consumerEmail ?
                            <section>
                                {this.state.loading ?
                                    <ClipLoader
                                        size={'2vh'}
                                        color={"#123abc"}
                                        loading={this.state.loading}
                                    />

                                    :
                                    <section
                                        className='advertisement-remove-from-consumer-list'
                                    >

                                        {this.props.consumerAd ?

                                            <section
                                                style={{ width: '3vh', height: '3vh' }}
                                                onClick={this.handleRemoveAdFromConsumer}
                                            >
                                                <img src={bookmarkEmpty} style={{ height: '3vh', width: '3vh' }} alt="fireSpot" />
                                            </section>
                                            :
                                            <section
                                                style={{ width: '3vh', height: '3vh' }}
                                                onClick={this.handleAddAdForConsumer}
                                            >
                                                <img src={bookmark} style={{ height: '3vh', width: '3vh' }} alt="fireSpot" />
                                            </section>

                                        }



                                    </section>

                                }
                         
                            </section>

                            :
                            <section

                                style={(typeof this.props.fullScreen != 'undefined' && this.props.fullScreen) ? { zIndex: 0, fontSize: '3vh', textDecoration: 'underline', position: 'absolute', bottom: '3vh' } : { zIndex: 0, fontSize: '2vh', textDecoration: 'underline' }}

                                onClick={this.registrationClick}
                            >
                                הרשמה

                            </section>

                        }
                    </section>
                </section>


            </section>








        )
    }
}

export default Advertisement;
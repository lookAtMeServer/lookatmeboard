import React from 'react';
import Slider from "react-slick";
import './Advertisement.css'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

const imageIntervalTime = 5000

class AdvertisementZoonIn extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            advertisements: [],
            advertisementImageIndex: 1
        }
    }
    componentDidMount() {
        if (typeof this.props.ad.pictures != 'undefined') {
            this.setState({
                advertisements: [...this.props.ad.pictures],
                intervalId: setInterval(() => {
                    this.handleSwitchPicture()
                }, imageIntervalTime)
            })
        }
    }

    componentWillUnmount() {
        clearInterval(this.state.intervalId)
    }

    handleSwitchPicture = () => {
        this.setState({
            advertisementImageIndex: this.state.advertisementImageIndex + 1
        })
    }


    render() {
        return (
            <section
                className={`advertisement-box ${this.props.match ? 'match-advertisement' : null}`}
                style={{  borderColor: this.props.ad.color, borderWidth: '2px', borderStyle: 'solid', height: '80vh' }}

            >
                {this.state.advertisements.length > 0 ?
                    <section onClick={this.handleSwitchPicture}
                        className='advertisement-image-box-modal'
                        style={{ backgroundImage: `url(http://lookatmeil.com/${this.state.advertisements[this.state.advertisementImageIndex % this.state.advertisements.length]})` }}
                    >
                    </section>
                    :
                    null
                }
                <section className='advertisement-info-modal'>
                    <section
                        style={{ fontSize: '3vh', fontWeight: 'bold',direction:'rtl' }}
                    >
                        {this.props.ad.categoryText}
                    </section>

                    <section
                        style={{ fontSize: '2vh',direction:'rtl' }}
                    >
                        <span style={{ fontWeight: 'bold' }}>שם מקום:</span>  {this.props.ad.placeName}
                    </section>

                    <section
                        style={{ fontSize: '2vh',direction:'rtl' }}
                    >
                        <span style={{ fontWeight: 'bold' }}>כתובת:</span>  {this.props.ad.serviceAddress}
                    </section>

                    <section
                        style={{ fontSize: '2vh',direction:'rtl' }}
                    >
                        <span style={{ fontWeight: 'bold' }}>טלפון:</span>  {this.props.ad.contactPhoneNumber}
                    </section>
                    <section
                        style={{ fontSize: '2vh',direction:'rtl' }}
                    >
                        <span style={{ fontWeight: 'bold' }}>מחיר:</span>  {this.props.ad.price}
                    </section>

                    <section
                        style={{ fontSize: '2vh'}}
                        className={'modal-description'}
                    >
                        <span style={{ fontWeight: 'bold',flexWrap:'wrap',textAlign:'right' }}>:תיאור</span>  {this.props.ad.serviceDescription}
                    </section>

                
                
                </section>
            </section>
        )
    }
}

export default AdvertisementZoonIn;
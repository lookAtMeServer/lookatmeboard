import React from 'react'
import './Error.css'

class Error extends React.Component{
    constructor(props){
        super(props)
    }
    render(){

        return(
        <p className='error-text'>{this.props.errorText}</p>
        )
    }

}

export default Error
import React from 'react'
import Api from '../assets/Api'
import Error from '../components/general/Error'
const nodeFetch = require('node-fetch')



class LogInSmartScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cities: [],
            digitalBoards: [],
            digitalBoardInfo: [],
            error: false
        }

        this.mountFlag = false
    }

    componentWillUnmount() {
        this.mountFlag = false
    }


    componentDidMount() {
        this.mountFlag = true
        this.loadAllCities()
    }

    handleSelectDigitalBoard = (event) => {
        let select = event.target
        let option = select.options[select.selectedIndex]
        if (option.value == -1 && this.mountFlag) {
            this.setState({
                digitalBoardInfo: [],
                error: 'לא נבחר לוח דיגיטלי'
            })
        } else {
            // console.log(option.value)
            // console.log(option.text)
            if (this.mountFlag) {

                this.setState({
                    digitalBoardInfo: {
                        digitalBoardId: option.value,
                        digitalBoardAddress: option.text,
                        error: false
                    }
                })

            }
        }
    }

    loadAllCities = async () => {
        try {
            let getAllCitiesResponed = await Api.getAllCities()
            if (this.mountFlag) {

                this.setState({
                    cities: [...getAllCitiesResponed.value],
                    error: false
                })

            }
        } catch (error) {
            if (this.mountFlag) {

                this.setState({
                    error: error.toString()
                })

            }

        }
    }

    selectInputOnChange = async (event) => {
        let select = event.target
        let option = select.options[select.selectedIndex]
        if (option.value == -1 && this.mountFlag) {
            this.setState({
                digitalBoardInfo: [],
                digitalBoards: [],
                error: 'לא נבחרה עיר'
            })
        } else {
            try {
                let getAllDigitalBoardsResponed = await Api.getAllDigitalBoards(option.value)

                if (this.mountFlag) {
                    this.setState({
                        digitalBoards: [...getAllDigitalBoardsResponed.value],
                        error: false
                    })

                }
            } catch (error) {
                if (this.mountFlag) {
                    this.setState({
                        error: error.toString()
                    })

                }
            }
        }
    }

    handleLogInButtom = async () => {
        if (this.citiesSelect.options[this.citiesSelect.selectedIndex].value != -1 && this.addressSelect.options[this.addressSelect.selectedIndex].value != -1) {

            if (this.state.digitalBoards && this.state.digitalBoardInfo) {
                this.props.handleLogInSmartScreen(this.state.digitalBoardInfo)
            }
        } else {
            if (this.mountFlag) {

                this.setState({
                    error: 'יש לבחור עיר ולוח דיגיטלי'
                })

            }
        }





    }

    render() {
        return (
            <section>
                <p>חבר מסך ללוח דיגיטלי</p>
                {this.state.error ? <Error errorText={this.state.error} /> : null}
                <section id='log-in-box'>
                    <select ref={ref => this.citiesSelect = ref} className='select-css' name="cities" id="cities" onChange={this.selectInputOnChange}>
                        <option value='-1'>בחר עיר</option>
                        {this.state.cities.map(item => <option key={item.id} point={{ id: item.id, city: item.city, latitude: item.latitude, longitude: item.longitude }} value={item.id}>{item.city}</option>)}
                    </select>

                    <select ref={ref => this.addressSelect = ref} className='select-css' name="address" id="address" onChange={this.handleSelectDigitalBoard}>
                        <option value='-1'>בחר לוח דיגיטלי</option>
                        {this.state.digitalBoards.map(item => <option key={item.id} value={item.id}>{item.address}</option>)}
                    </select>


                    <button id='log-in-button' onClick={this.handleLogInButtom}>
                        התחבר
                    </button>

                </section>

            </section>
        )
    }
}


export default LogInSmartScreen
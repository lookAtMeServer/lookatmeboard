import React from 'react'
import LogInSmartScreen from '../screens/LogInSmartScreen'


class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.loadPage()


    }

    componentWillUnmount(){
    }


    loadPage = () => {
        if (typeof this.props.digitalBoardId != 'undefined' && typeof this.props.smartScreenId != 'undefined') {
            if (!this.props.digitalBoardId || !this.props.digitalBoardId) {
                return <LogInSmartScreen
                    handleLogInSmartScreen={this.props.handleLogInSmartScreen}
                    handleError={this.props.handleError}
                />
            }
        } else {
            this.props.handleError('digitalBoardId, smartScreenId לא מוצא משתנים ')

        }

    }

    render() {
        return (
            <section>
                {this.loadPage()}
            </section>
        )
    }
}

export default HomeScreen
import React from 'react'
import Advertisement from '../components/Advertisement'
import AdvertisementZoonIn from '../components/AdvertisementZoonIn'
import Api from '../assets/Api'
import Error from '../components/general/Error'
import Modal from 'react-modal'
import ClipLoader from "react-spinners/ClipLoader"
import Consumer from '../assets/consumer'
import DigitalBoard from '../assets/digitalBoard'
const validator = require('validator')


const testFlag = false




const signUpModal = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};
const zoonInModal = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '80%',
        height: '95%'
    }
}





Modal.setAppElement('#root')



class AdvertisementScreen extends React.Component {
    constructor() {
        super()
        this.state = {
            advertisements: [],
            consumerAdvertisements: [],
            error: false,
            consumerEmail: false,
            categories: [],
            filterAdvertisements: [],
            filterConsumerAdvertisements: [],
            selectCategory: false,
            getConsumerSelectedAds: [],
            signUpModal: false,
            signUpModalAdId: false,
            zoomInModal: false,
            zoomInModalAdInfo: false,
            noUserNearByPictureIndex: 0,
            interval: false
        }
        this.randomColors = []
        this.mountFlag = false
    }

    componentDidUpdate() {
        if (this.state.consumerEmail != this.props.consumerEmail) {
            this.setState({
                consumerEmail: this.props.consumerEmail
            })
            this.randerAdvertisements()

        }
    }



    randerAdvertisements = async () => {
        try {

            let advertisements = await DigitalBoard.getAdvertisements()
            let categories = await DigitalBoard.filterCategories(advertisements)
            let matchAdsForConsumer = false
            if (this.state.consumerEmail) {
                matchAdsForConsumer = await DigitalBoard.getMatchAdsForConsumer(this.props.consumerEmail)
                DigitalBoard.filterAllCategories(advertisements, matchAdsForConsumer)
                DigitalBoard.setMatchAds(advertisements, matchAdsForConsumer)

                matchAdsForConsumer.sort((a, b) => a.weight - b.weight)
                matchAdsForConsumer.sort((a, b) => a.categoryId - b.categoryId)

                let consumerSelectedAds = await Api.getConsumerSelectedAds(this.state.consumerEmail)
                DigitalBoard.setConsumerSelectedAds(consumerSelectedAds, advertisements, matchAdsForConsumer)
            }


            let filterAdvertisements = []
            let filterConsumerAdvertisements = []
            if (this.state.selectCategory) {
                DigitalBoard.selectCategory(this.state.selectCategory, filterAdvertisements, filterConsumerAdvertisements, advertisements, matchAdsForConsumer, this.state.consumerEmail)
            }

            if (this.state.consumerEmail && matchAdsForConsumer.length > 0) {
                matchAdsForConsumer = [...DigitalBoard.secondFilterMatch(matchAdsForConsumer)]
            }
            if (this.state.consumerEmail && filterConsumerAdvertisements.length > 0) {
                filterConsumerAdvertisements = [...DigitalBoard.secondFilterMatch(filterConsumerAdvertisements)]
            }

            if (advertisements.length > 0) {
                advertisements = [...DigitalBoard.secondFilterAds(advertisements)]
            }
            if (matchAdsForConsumer.length > 0) {
                matchAdsForConsumer = [...DigitalBoard.secondFilterAds(matchAdsForConsumer)]
            }




            this.setState({
                advertisements: [...advertisements],
                categories: [...categories],
                consumerAdvertisements: this.state.consumerEmail ? [...matchAdsForConsumer] : [],
                filterAdvertisements: [...filterAdvertisements],
                filterConsumerAdvertisements: [...filterConsumerAdvertisements]
            })



        } catch (error) {
            console.log(error)
            this.setState({
                error: error.toString()
            })
        }
    }

    componentDidMount() {

        if (!testFlag && !this.state.interval) {
            let interval = setInterval(() => {
                this.setState({
                    noUserNearByPictureIndex: this.state.noUserNearByPictureIndex + 1
                })
            }, 5000)
            this.setState({
                interval: interval
            })
        }

        this.mountFlag = true
        DigitalBoard.setDigitalBoardId(this.props.digitalBoardInfo.digitalBoardId)
        this.randerAdvertisements()

    }

    componentWillUnmount() {
        this.mountFlag = false
        if (this.state.interval) {
            clearInterval(this.state.interval)
        }
    }

    handleSelectCategory = async () => {
        let selectedCategoryId = this.selectCategory.options[this.selectCategory.selectedIndex].value
        if (selectedCategoryId != -1) {
            let filterAdvertisements = []
            let filterConsumerAdvertisements = []
            if (this.state.consumerEmail) {
                filterConsumerAdvertisements = this.state.consumerAdvertisements.filter(item => item.categoryId == selectedCategoryId)
            }
            filterAdvertisements = this.state.advertisements.filter(item => item.categoryId == selectedCategoryId)
            this.setState({
                filterAdvertisements: [...filterAdvertisements],
                filterConsumerAdvertisements: [...filterConsumerAdvertisements],
                selectCategory: selectedCategoryId
            })
        } else {
            this.setState({
                selectCategory: false
            })
        }
    }

    openSignUpModal = async (advertisementId) => {
        console.log('pppppp')
        this.setState({
            signUpModal: true,
            signUpModalAdId: advertisementId
        })
    }
    closeSignUpModal = () => {
        this.setState({
            signUpModal: false,
            signUpModalAdId: false
        })
    }
    openZoomInModal = async (advertisementInfo) => {
        await this.timerOnOff(false)
        this.setState({
            zoomInModal: true,
            zoomInModalAdInfo: { ...advertisementInfo }
        })
    }
    closeZoomInModal = async () => {
        await this.timerOnOff(true)
        this.setState({
            zoomInModal: false,
            zoomInModalAdInfo: false
        })
    }

    handleSignUpSubmit = async () => {
        let validationFlag = true


        if (validator.isEmail(this.emailInput.value)) {
            this.emailInput.classList.remove('not-valid')
            this.emailInput.classList.add('valid')
        } else {
            this.emailInput.classList.remove('valid')
            this.emailInput.classList.add('not-valid')
            validationFlag = false
        }

        if (validationFlag) {
            try {
                this.setState({
                    loading: true
                })
                let sendSignUpEmailResponed = await Api.sendSignUpEmail(this.emailInput.value, this.state.signUpModalAdId)
                this.setState({
                    loading: false,
                    signUpModal: false,
                    signUpModalAdId: false
                })
            } catch (error) {
                this.setState({
                    error: error.toString(),
                    loading: false
                })
            }
        }


    }

    getAdvertisementByIndex = () => {



    }

    timerOnOff = async (bool) => {
        if (!bool) {
            let index = this.state.noUserNearByPictureIndex
            if (this.state.interval) {
                await new Promise(resolved => {
                    clearInterval(this.state.interval)
                    this.setState({
                        interval: false
                    }, () => resolved())

                })
                if (this.mountFlag) {
                    await new Promise(resolved => {
                        this.setState({
                            noUserNearByPictureIndex: index
                        }, () => resolved())
                    })
                }
            }

        } else {
            if (!this.state.interval) {
                await new Promise(resolved => {
                    let interval = setInterval(() => {
                        if (this.mountFlag) {
                            this.setState({
                                noUserNearByPictureIndex: this.state.noUserNearByPictureIndex + 1
                            })


                        }
                    }, 5000)
                    if (this.mountFlag) {

                        this.setState({
                            interval: interval
                        }, () => resolved())

                    }

                })
            }

        }
    }

    render() {



        console.log(this.state.consumerAdvertisements)
        console.log(this.state.advertisements)
        return (
            <section className='consumer-box'>
                <Modal
                    isOpen={this.state.signUpModal}
                    onRequestClose={this.closeSignUpModal}

                >
                    <section
                        className={'sign-up-box'}
                    >

                        <section id='sign-up-close-button-box'

                            onClick={this.closeSignUpModal}
                        >
                            סגור

                        </section>


                        <section id='sign-up-form-box' >

                            <label >הכנס מייל</label>
                            <input type='email' id='email-input' ref={ref => this.emailInput = ref} />
                            <section id='sign-up-close-next-button'

                                onClick={this.handleSignUpSubmit}
                            >
                                {this.state.loading ?
                                    <ClipLoader
                                        size={20}
                                        color={"#123abc"}
                                        loading={this.state.loading}
                                    />
                                    :
                                    <p>הירשם</p>

                                }

                            </section>

                        </section>






                    </section>



                </Modal>
                <Modal
                    isOpen={this.state.zoomInModal}
                    onRequestClose={this.closeZoomInModal}

                >
                    <section
                    id={'modal-ad-box'}
                        style={{ height: '80vh',width:'85vw', zIndex: 400, position: 'relative',alignItems:'0 auto' }}
                    >
                        <section
                            onClick={this.closeZoomInModal}
                            style={{ position: 'absolute', top: -3, left: '0.5vw', zIndex: 2000, fontSize: '5vw', lineHeight: '3vw' }}
                        >
                            x
                        </section>


                        <section>
                            <AdvertisementZoonIn
                                match={false}
                                ad={this.state.zoomInModalAdInfo}
                                consumerEmail={this.state.consumerEmail}

                            />
                        </section>

                    </section>



                </Modal>
                <section className='advertisements-top-section'>
                    {this.props.consumerEmail ?
                        <section className='consumer-login-box'>
                            <section id='consumer-logoff-text' onClick={this.props.handleConsumerLogOff.bind(null, this.props.consumerEmail)}>
                                {this.props.consumerPicture ?
                              

                                    <img id={'consumer-picture'} src={`http://lookatmeil.com/${this.props.consumerPicture}`} width="10vh" height="10vh"></img>
                                    :
                                    <p style={{color:'black'}}> 

                                        {this.props.consumerEmail}
                                    </p>

                                }
                                <section>
                                התנתק

                                </section>
                                </section>
  
                        </section>
                        :
                        null
                    }
                    <section id='select-category-box'
                        style={{ fontSize: '5vw' }}
                    >
                        <select ref={ref => this.selectCategory = ref} id="select-category" onChange={this.handleSelectCategory}>
                            <option value='-1'>בחר קטגוריה</option>
                            {this.state.categories.map(item => <option key={item.categoryId} value={item.categoryId}>{item.categoryText}</option>)}
                        </select>
                    </section>


                </section>
                {this.state.error ? <Error errorText={this.state.error} /> : null}
                {this.state.selectCategory ?
                    <section className='advertisement-frame'>


                        {this.state.filterConsumerAdvertisements.map(ad => <Advertisement
                            consumerAd={ad.consumerAd}
                            match={ad.match}
                            key={ad.id.toString()}
                            ad={ad}
                            consumerEmail={this.state.consumerEmail}
                            openSignUpModal={this.openSignUpModal}
                            openZoomInModal={this.openZoomInModal}
                            randerAdvertisements={this.randerAdvertisements}
                        />)}
                        {this.state.filterAdvertisements.map(ad => <Advertisement
                            consumerAd={ad.consumerAd}
                            match={ad.match}
                            key={ad.id.toString()}
                            ad={ad}
                            consumerEmail={this.state.consumerEmail}
                            openSignUpModal={this.openSignUpModal}
                            openZoomInModal={this.openZoomInModal}
                            randerAdvertisements={this.randerAdvertisements}
                        />)}






                    </section>
                    :
                    <section style={{ width: '100%' }} >
                        {this.state.consumerEmail ?
                            <section className='advertisement-frame'>
                                {this.state.consumerAdvertisements.map(ad => <Advertisement
                                    consumerAd={ad.consumerAd}
                                    match={ad.match}
                                    key={ad.id.toString()}
                                    ad={ad}
                                    consumerEmail={this.state.consumerEmail}
                                    openSignUpModal={this.openSignUpModal}
                                    openZoomInModal={this.openZoomInModal}
                                    randerAdvertisements={this.randerAdvertisements}
                                />)}
                                {this.state.advertisements.map(ad => <Advertisement
                                    consumerAd={ad.consumerAd}
                                    match={ad.match}
                                    key={ad.id.toString()}
                                    ad={ad}
                                    consumerEmail={this.state.consumerEmail}
                                    openSignUpModal={this.openSignUpModal}
                                    openZoomInModal={this.openZoomInModal}
                                    randerAdvertisements={this.randerAdvertisements}
                                />)}

                            </section>

                            :
                            <section style={{ width: '100%' }}>
                                {this.state.advertisements.length > 0 ?
                                    <section style={{ width: '100%' }}>
                                        {(typeof this.state.advertisements[this.state.noUserNearByPictureIndex % (this.state.advertisements.length)] != 'undefined' &&
                                            this.state.advertisements[this.state.noUserNearByPictureIndex % (this.state.advertisements.length)]) ?

                                            <Advertisement
                                                consumerAd={this.state.advertisements[this.state.noUserNearByPictureIndex % (this.state.advertisements.length)].consumerAd}
                                                match={this.state.advertisements[this.state.noUserNearByPictureIndex % (this.state.advertisements.length)].match}
                                                key={this.state.advertisements[this.state.noUserNearByPictureIndex % (this.state.advertisements.length)].id.toString()}
                                                ad={this.state.advertisements[this.state.noUserNearByPictureIndex % (this.state.advertisements.length)]}
                                                consumerEmail={this.state.consumerEmail}
                                                openSignUpModal={this.openSignUpModal}
                                                openZoomInModal={this.openZoomInModal}
                                                randerAdvertisements={this.randerAdvertisements}
                                                fullScreen={true}
                                                timerOnOff={this.timerOnOff}

                                            />
                                            :

                                            <p></p>
                                        }




                                    </section>


                                    :
                                    <section>

                                        <p>אין מודעות להציג</p>
                                    </section>
                                }




                            </section>

                        }


                    </section>
                }
            </section>

        )
    }




}

export default AdvertisementScreen